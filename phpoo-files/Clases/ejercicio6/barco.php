<?php
include_once('transporte.php');

    class barco extends transporte{
		private $calado;

		//declaracion de constructor
		
		public function __construct($nom,$vel,$com,$cal){
			//sobreescritura de constructor
			parent::__construct($nom,$vel,$com);
			$this->calado=$cal;
		}

		// declaracion y sobreescritura de metodo
		public function resumenBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Calado:</td>
						<td>'. $this->calado.'</td>				
					</tr>';
			return $mensaje;
		}
	}

    
?>