<?php
    include_once('transporte.php');

    class cohete extends transporte{

        private $numero_motores;

        //declaracion del constructor
        public function __construct($nom,$vel,$com,$mot){
            //sobresescritura de constructor de la clase padre
            parent::__construct($nom,$vel,$com);
            $this->numero_motores=$mot;

        }

        //declaracion del método

        public function resumenCohete(){
            //sobrescritura de metodo crear_ficha de la clase padre
            $mensaje=parent::crear_ficha();
            $mensaje.='<tr>
						<td>Numero de puertas:</td>
						<td>'. $this->numero_motores.'</td>				
					</tr>';
			return $mensaje;
        }
    }
?>