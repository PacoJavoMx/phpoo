<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto{
	//declaracion de las propiedades
	public $marca;
	public $modelo;
}

//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor2='';
$mensajeServidor3='';

//crea aqui la instancia o el objeto de la clase Moto
$Moto1 = new Moto;

 if ( !empty($_POST)){
 	 // recibe aqui los valores mandados por post y arma el mensaje para front 
	
	// se almacena el valor mandado por POST en el atributo marca
	$Moto1->marca=$_POST['marca'];
	// se construye el mensaje que sera lanzado por el servidor
	$mensajeServidor2='El servidor dice que ya se escogio una marca: ' .$_POST['marca'];

	// se almacena el valor mandado por POST en el atributo modelo
	$Moto1->modelo=$_POST['modelo'];
	// se construye el mensaje que sera lanzado por el servidor
	$mensajeServidor3='El servidor dice que ya se escogio una modelo: ' .$_POST['modelo'];
 }  


?>
