<?php
    
    
    class token2{
            //Declaracion de atributos

            public $nombre;
            public $password;

            //Declaracion de constructor con parametro para los atributos

            
            public function __construct ($nombreusuario){
                $this -> nombre = $nombreusuario;
                $this -> password = rand();
            }

            //Metodo mostrar
            public function mostrar(){
                return 'Hola '.$this->nombre.' este es tu contraseña: '.$this->password;
            }

            //declaracion de metodo destructor
		    public function __destruct(){
			//destruye token
			$this->password='La contraseña ' .$this->password. ' ha sido destruido';
			echo $this->password;
		}
	}
    
    $mensaje='';

    if (!empty($_POST)){
        //creacion de objeto de la clase
        $token= new token2($_POST['nombre']);
        $mensaje=$token->mostrar();
    }

    
?>
