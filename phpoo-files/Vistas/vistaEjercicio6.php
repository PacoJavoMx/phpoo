<?php  
include_once('../clases/ejercicio6/carro.php');
include_once('../clases/ejercicio6/barco.php');
include_once('../clases/ejercicio6/avion.php');
include_once('../clases/ejercicio6/cohete.php');


?>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Los transportes</h1></header><br>
	<form method="post">
		

					 <div class="form-group">
				 		<label for="CajaTexto1">Tipo de transporte:</label>
						<select class="form-control" name="tipo_transporte" id="CajaTexto1">
							<option value='aereo' >Aereo</option>
							<option value='terrestre' >Terrestre</option>
							<option value='maritimo' >Maritimo</option>
							<option value='espacial' >Espacial</option>
						</select>
					</div>

					
			
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>
	
	<?php
	if ($_POST['tipo_transporte']=='aereo') {
		$jet1 = new avion('jet','400','gasoleo','2');
		$mensaje = $jet1->resumenAvion();
		}
		
	?>
	<?php
	if($_POST['tipo_transporte']=='terrestre') {
		$carro1 = new carro('carro','260','gasolina','4');
		$mensaje = $carro1->resumencarro();
		}
	?>
	
	<?php
	if($_POST['tipo_transporte']=='maritimo') {
				$bergantin1 = new barco('bergantin','40','na','15');
				$mensaje = $bergantin1->resumenbarco();	
	}			
	?>

	<?php
	if($_POST['tipo_transporte']=='espacial'){

			$discover1 = new cohete ('Discover','120000','Hidrogeno y Queroseno','4');
			$mensaje = $discover1 -> resumenCohete();
	}
	?>


	
	
	</div>
	<div class="container mt-5">
		<h1>Respuesta del servidor</h1>
		<table class="table">
			<thead>
		      <tr>
		      	 <th>Transporte</th>
		      </tr>
		    </thead>
		    <tbody>
			<?= $mensaje; ?>

			</tbody>
		</table>

    </div>
	


</body>
</html>
